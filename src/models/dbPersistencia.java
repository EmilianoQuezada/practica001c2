package models;
import java.util.ArrayList;
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void habilitar(Object objeto, String codigo) throws Exception;
    public void deshabilitar(Object objeto, String codigo) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listarDes() throws Exception;
    
    public Object buscarDes(String codigo) throws Exception;
    public Object buscar(String codigo) throws Exception;
}
