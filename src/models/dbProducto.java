package models;

import java.sql.SQLException;
import java.util.ArrayList;

public class dbProducto extends DbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "";
        consulta = "insert into " + "productos(codigo,nombre,precio,fecha,status)values(?,?,?,?,?)";

        if (this.Conectar()) {
            try {
                System.err.println("se conecto");

                this.sqlConsulta = conexion.prepareStatement(consulta);
                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setFloat(3, pro.getPrecio());
                this.sqlConsulta.setString(4, pro.getFecha());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar; " + e.getMessage());
            }
        }

    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;

        String consulta = "update productos set nombre = ?, precio = ?, " + "fecha =? where codigo = ?";

        if (this.Conectar()) {
            try {
                System.err.println("se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);

                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
            } catch (SQLException e) {
                System.err.println("Surgio un error al actualizar: " + e.getMessage());
            }
        }
        this.Desconectar();
    }

    @Override
    public void habilitar(Object objeto, String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "UPDATE productos set status = 0 where codigo=? AND status = 1";
        if (Conectar()) {
            try {
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();

            } catch (SQLException e) {
                System.err.println("Surgio un erroe al desahibilitar: " + e.getMessage());
            }
        }

        Desconectar();
    }

    @Override
    public void deshabilitar(Object objeto, String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "UPDATE productos set status = 1 where codigo=? AND status = 0";
        if (Conectar()) {
            try {
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();

            } catch (SQLException e) {
                System.err.println("Surgio un erroe al desahibilitar: " + e.getMessage());
            }
        }

        Desconectar();
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean res = false;
        Productos pro = new Productos();
        if (this.Conectar()) {
            String consulta = "select * from productos where codigo =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;

        if (this.Conectar()) {
            String consulta = "SELECT * from productos where status = 0 order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProductos"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.Desconectar();;
        return lista;
    }

    @Override
    public ArrayList listarDes() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;

        if (this.Conectar()) {
            String consulta = "SELECT * from productos where status = 1 order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProductos"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.Desconectar();;
        return lista;
    }

    @Override
    public Object buscarDes(String codigo) throws Exception {
        Productos pro = new Productos();
        if (this.Conectar()) {
            String consulta = "SELECT * from productos where codigo = ? and status =1";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.Desconectar();
        return pro;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if (this.Conectar()) {
            String consulta = "SELECT * from productos where codigo = ? and status =0";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //Asignar valores
            this.sqlConsulta.setString(1, codigo);
            //Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            if (this.registros.next()) {
                pro.setIdProducto(this.registros.getInt("idProductos"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.Desconectar();
        return pro;
    }

}
