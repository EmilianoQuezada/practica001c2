package models;
import java.util.ArrayList;
public class TestDb {

    public static void main(String[] args) {
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        pro.setCodigo("10010");
        pro.setNombre("Avena");
        pro.setPrecio(34.60f);
        pro.setFecha("2023-06-22");
        pro.setStatus(0);
        
        
        try{
            db.insertar(pro);
            System.out.println("SE AGREGO CON EXITO");
            
            Productos res = new Productos();
            res=(Productos) db.buscar("10010");
            System.out.println("Nombre:"+ res.getNombre() + " Precio:"+ res.getPrecio());
            
        }
        catch(Exception e){
            System.err.println("Surgio un error " + e.getMessage());
        }
    }
    
}
