package controles;
import models.dbProducto;
import models.Productos;
import views.dlgProductos;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLDataException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import java.util.Date;


public class Controlador implements ActionListener{
    
    private Productos productos;
    private dbProducto comandos;
    private dlgProductos vista;
    private boolean existe;

    public Controlador(Productos productos, dlgProductos vista, dbProducto comandos) {
        this.productos = productos;
        this.vista = vista;
        this.comandos = comandos;
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnDeshabilitar.addActionListener(this);
        this.vista.btnBuscar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
        this.vista.btnBuscarDes.addActionListener(this);
        this.vista.btnHabilitar.addActionListener(this);
    }
    
    public void iniciarVista(){
        llenadoTabla();
        llenadoTablaDes();
        this.vista.setTitle("PRODUCTOS");
        this.vista.setSize(440, 535);
        this.vista.setVisible(true);
    }
    
    public void limpiar(){
        this.vista.txtId.setText("");
        this.vista.txtCodigo.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtPrecio.setText("");
        this.vista.dtcFecha.setDate(null);
        this.vista.cbStatus.setSelectedIndex(0);
        
        this.vista.txtCodigoDes.setText("");
        this.vista.txtNombreDes.setText("");
    }
    
    public void llenadoTabla(){
        DefaultTableModel tablaProductos = new DefaultTableModel();
        ArrayList<Productos> lista = new ArrayList<>();
        try{
            lista = comandos.listar();
        }
        catch(Exception e){
            
        }
        tablaProductos.addColumn("Id");
        tablaProductos.addColumn("Codigo");
        tablaProductos.addColumn("Nombre");
        tablaProductos.addColumn("Fecha");
        tablaProductos.addColumn("Precio");
        for(Productos producto: lista ){
            tablaProductos.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(),producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tblProductos.setModel(tablaProductos);
    }
    
    public void llenadoTablaDes(){
        DefaultTableModel tablaProductosDes = new DefaultTableModel();
        ArrayList<Productos> lista2 = new ArrayList<>();
        try{
            lista2 = comandos.listarDes();
        }
        catch(Exception e){
            
        }
        tablaProductosDes.addColumn("Id");
        tablaProductosDes.addColumn("Codigo");
        tablaProductosDes.addColumn("Nombre");
        tablaProductosDes.addColumn("Fecha");
        tablaProductosDes.addColumn("Precio");
        for(Productos producto: lista2 ){
            tablaProductosDes.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(),producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tblProductosDes.setModel(tablaProductosDes);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(e.getSource()== vista.btnNuevo){
            this.vista.txtCodigo.enable(true);
            this.vista.txtNombre.enable(true);
            this.vista.txtPrecio.enable(true);
            this.vista.dtcFecha.setEnabled(true);
            this.vista.cbStatus.setEnabled(true);
            this.vista.btnGuardar.setEnabled(true);
            this.vista.btnBuscar.setEnabled(true);
            this.vista.btnDeshabilitar.setEnabled(true);
            this.vista.btnLimpiar.setEnabled(true);
            this.vista.btnCancelar.setEnabled(true);
            
            this.vista.txtCodigoDes.enable(true);
            this.vista.txtNombreDes.enable(true);
            this.vista.btnHabilitar.setEnabled(true);
        }
        
        if(e.getSource()==vista.btnGuardar){
            try{
                productos.setIdProducto(0);
                productos.setCodigo(vista.txtCodigo.getText());
                productos.setNombre(vista.txtNombre.getText());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
                String fecha = sdf.format(vista.dtcFecha.getDate());
                productos.setFecha(fecha);
                productos.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                productos.setStatus((int)vista.cbStatus.getSelectedIndex());
                if(!comandos.isExiste(vista.txtCodigo.getText())){
                    comandos.insertar(productos);
                    JOptionPane.showMessageDialog(vista, "Guardado correctamente");
                    llenadoTabla();
                    llenadoTablaDes();
                }
                else{
                    comandos.actualizar(productos);
                    JOptionPane.showMessageDialog(vista, "Actualizado correctamente");
                    llenadoTabla();
                    llenadoTablaDes();
                }
                
            }
            catch (SQLDataException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }

        }
        
        if(e.getSource()==vista.btnBuscar){
            try{
                Productos res = new Productos();
                String codigo = vista.txtCodigo.getText();
                existe= comandos.isExiste(codigo);
                if(comandos.isExiste(codigo)){
                    res = (Productos) comandos.buscar(codigo);
                    vista.txtNombre.setText(res.getNombre());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
                    Date fecha = sdf.parse(res.getFecha());
                    vista.dtcFecha.setDate(fecha);
                    vista.txtPrecio.setText(String.valueOf(res.getPrecio()));
                    vista.cbStatus.setSelectedIndex(res.getStatus());
                    vista.cbStatus.setEnabled(false);
                    JOptionPane.showMessageDialog(vista, "Producto encontrado");
                    
                }
                else{
                    JOptionPane.showMessageDialog(vista, "Producto no encontrado");
                    limpiar();
                }
            }
            catch(Exception ex){
                System.out.println("Surgio el error: "+ ex.getMessage());
                System.out.println(existe);
            }
        }
        
        if(e.getSource()==vista.btnBuscarDes){
            try{
                Productos res= new Productos();
                String codigo = vista.txtCodigoDes.getText();
                if(comandos.isExiste(codigo)){
                    res = (Productos) comandos.buscarDes(codigo);
                    vista.txtNombreDes.setText(res.getNombre());
                    JOptionPane.showMessageDialog(vista, "Producto encontrado");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "Producto no encontrado");
                    limpiar();
                }
            }
            catch(Exception ex){
                System.out.println("Surgio el error: "+ ex.getMessage());
                
            }
        }
        
        if(e.getSource()==vista.btnDeshabilitar){
            try{
                comandos.deshabilitar(productos, vista.txtCodigo.getText());
                llenadoTabla();
                llenadoTablaDes();
            }
            catch(Exception ex){
                System.out.println("Surgio el error: "+ ex.getMessage());
            }
        }
        
        if(e.getSource()==vista.btnHabilitar){
            try{
                comandos.habilitar(productos, vista.txtCodigoDes.getText());
                llenadoTabla();
                llenadoTablaDes();
            }
            catch(Exception ex){
                System.out.println("Surgio el error: "+ ex.getMessage());
            }
        }
        
        if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource()==vista.btnCancelar){
            limpiar();
            
            this.vista.txtCodigo.enable(false);
            this.vista.txtNombre.enable(false);
            this.vista.dtcFecha.setEnabled(false);
            this.vista.txtPrecio.enable(false);
            this.vista.cbStatus.setEnabled(false);
            this.vista.btnBuscar.setEnabled(false);
            this.vista.btnGuardar.setEnabled(false);
            this.vista.btnDeshabilitar.setEnabled(false);
            this.vista.btnLimpiar.setEnabled(false);
            this.vista.btnCancelar.setEnabled(false);
            
            this.vista.txtCodigoDes.enable(false);
            this.vista.txtNombreDes.enable(false);
            this.vista.btnBuscarDes.setEnabled(false);
            this.vista.btnHabilitar.setEnabled(false);
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Desea cerrar el programa?",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args) {
        Productos productos = new  Productos();
        dbProducto comandos = new dbProducto();
        dlgProductos vista = new dlgProductos(new JFrame(), true);
        Controlador contra = new Controlador(productos, vista, comandos);
        contra.iniciarVista();
    }

    
}
